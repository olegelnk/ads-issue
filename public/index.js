/* 
  The refresh interval has been reduced specifically for testing. 
  In production, this is 60 seconds. 
*/
const REFRESH_INTERVAL = 5000;

var interval;
var counterBlock;

const refreshSlots = (slots, googletag) => {
  console.log("[ADS]: Ads is updated");
  counterBlock.innerText = Number(counterBlock.innerText) + 1;

  googletag.cmd.push(() => {
    googletag.pubads().refresh(slots);
  });
};

window.onload = () => {
  const { googletag } = window || {};
  googletag.cmd = googletag.cmd || [];

  let topAd;
  let leftAd;
  let rightAd;

  googletag.cmd.push(() => {
    topAd = googletag
      .defineSlot(
        "/9633201/elnk2_wm_728x90",
        [728, 90],
        "div-gpt-ad-1585752820590-0"
      )
      .setTargeting("interests", "travel")
      .addService(googletag.pubads());

    leftAd = googletag
      .defineSlot(
        "/9633201/elnk2_wm_left_160x600",
        [160, 600],
        "div-gpt-ad-1585752590473-0"
      )
      .setTargeting("interests", "travel")
      .addService(googletag.pubads());

    rightAd = googletag
      .defineSlot(
        "/9633201/elnk2_wm_right_160x600",
        [160, 600],
        "div-gpt-ad-1585752713359-0"
      )
      .setTargeting("interests", "travel")
      .addService(googletag.pubads());

    googletag.pubads().enableSingleRequest();
    googletag.pubads().disableInitialLoad();
    googletag.enableServices();
  });

  googletag.cmd.push(() => {
    googletag.display("div-gpt-ad-1585752820590-0");
    googletag.display("div-gpt-ad-1585752590473-0");
    googletag.display("div-gpt-ad-1585752713359-0");

    console.log("[ADS]: Ads is embedded");
  });

  counterBlock = document.querySelector("#counter");

  refreshSlots([topAd, leftAd, rightAd], googletag);

  interval = setInterval(
    () => refreshSlots([topAd, leftAd, rightAd], googletag),
    REFRESH_INTERVAL
  );
};

window.onunload = () => {
  if (interval) clearInterval(interval);
};
