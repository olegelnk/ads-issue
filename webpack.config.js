const path = require("path");

module.exports = {
  mode: "none",
  entry: "./public/index.js",
  output: {
    path: __dirname + "/public",
    filename: "bundle.js",
  },
  devServer: {
    static: {
      directory: path.join(__dirname, "public"),
    },
    port: 3000,
  },
};
